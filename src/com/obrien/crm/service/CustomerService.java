package com.obrien.crm.service;

import java.util.List;

import com.obrien.crm.entity.Customer;

public interface CustomerService {
	
	public List<Customer> getCustomers();
	
	public void saveCustomer(Customer customer);

	public Customer getCustomer(int custId);

	public void deleteCustomer(int custId);

	public List<Customer> searchCustomer(String searchName);

}

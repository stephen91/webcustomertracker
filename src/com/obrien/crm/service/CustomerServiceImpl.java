package com.obrien.crm.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.obrien.crm.dao.CustomerDao;
import com.obrien.crm.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService, CustomerDao {
	
	@Autowired
	private CustomerDao customerDao;
	
	@Transactional
	@Override
	public List<Customer> getCustomers() {
		return customerDao.getCustomers();
	}
	
	@Transactional
	@Override
	public void saveCustomer(Customer customer) {
		customerDao.saveCustomer(customer);
	}

	@Transactional
	@Override
	public Customer getCustomer(int custId) {
		return customerDao.getCustomer(custId);
	}
	
	@Transactional
	@Override
	public void deleteCustomer(int custId) {
		customerDao.deleteCustomer(custId);
	}
	
	@Transactional
	@Override
	public List<Customer> searchCustomer(String searchName) {
		return customerDao.searchCustomer(searchName);
	}

}

package com.obrien.crm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.obrien.crm.entity.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<Customer> getCustomers() {
		Session session = sessionFactory.getCurrentSession();
		
		Query<Customer> getQuery = session.createQuery("from Customer order by lastName", 
				Customer.class);
		
		return getQuery.getResultList();
	}

	@Override
	public void saveCustomer(Customer customer) {
		Session sesh = sessionFactory.getCurrentSession();
		sesh.saveOrUpdate(customer);
	}

	@Override
	public Customer getCustomer(int custId) {
		Session sesh = sessionFactory.getCurrentSession();
		return sesh.get(Customer.class, custId);
	}

	@Override
	public void deleteCustomer(int custId) {
		Session sesh = sessionFactory.getCurrentSession();
		Query query = sesh.createQuery("delete from Customer where id=:customerId").setParameter("customerId", custId);
		query.executeUpdate();
		
	}

	@Override
	public List<Customer> searchCustomer(String searchName) {
		Session sesh = sessionFactory.getCurrentSession();
		
		Query query;
		
		if(searchName !=null && searchName.trim().length() != 0) {
			query = sesh.createQuery("from Customer where lower(firstName) like:searchName or lower(lastName) like:searchName");
			query.setParameter("searchName", searchName.toLowerCase());
		} else {
			query = sesh.createQuery("from Customer");
		}
		
		return query.getResultList();
	}

}

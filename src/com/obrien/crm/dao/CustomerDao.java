package com.obrien.crm.dao;

import java.util.List;

import com.obrien.crm.entity.Customer;

public interface CustomerDao {
	
	List<Customer> getCustomers();

	void saveCustomer(Customer customer);

	Customer getCustomer(int custId);

	void deleteCustomer(int custId);

	List<Customer> searchCustomer(String searchName);

}

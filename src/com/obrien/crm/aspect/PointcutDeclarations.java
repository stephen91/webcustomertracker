package com.obrien.crm.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class PointcutDeclarations {
	
	@Pointcut("execution( * com.obrien.crm.controller.*.*(..))")
	private void forControllerPackage() {}
	
	@Pointcut("execution( * com.obrien.crm.service.*.*(..))")
	private void forServicePackage() {}
	
	@Pointcut("execution( * com.obrien.crm.dao.*.*(..))")
	private void forDaoPackage() {}
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
	public void end2endFlow() {}

}

package com.obrien.crm.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class CRMLoggingAspect {

	private final Logger LOGGER = Logger.getLogger(getClass().getName());

	@Before("com.obrien.crm.aspect.PointcutDeclarations.end2endFlow()")
	public void beforeLogging(JoinPoint joinPoint) {
		String methodName = joinPoint.getSignature().getName();

		LOGGER.info(String.format("**************** @Before Aspect for method: %s *****************", methodName));

		Object[] tempArgs = joinPoint.getArgs();

		for (Object tempArg : tempArgs) {
			LOGGER.info(String.format("%s has been called with this argument [%s]", methodName, tempArg));
		}

	}
	
	@AfterReturning(pointcut="com.obrien.crm.aspect.PointcutDeclarations.end2endFlow()",
			        returning="theResult")
	public void afterLogging(JoinPoint joinPoint, Object theResult) {
		
		String methodName = joinPoint.getSignature().getName();

		LOGGER.info(String.format("**************** @AfterReturning Aspect for method: %s *****************", methodName));
		
		LOGGER.info(String.format("%s has a return value of [%s]",methodName,  theResult));
	}
}
